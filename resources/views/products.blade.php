@extends('layout')

@section('title', 'Products')

@section('content')

    <div class="container products">

        <div class="row" style="width:100%; height: 300px !important;">
            @php
                $i = 1;
            @endphp
            @foreach($products as $product)
                <div class="col-xs-12 col-sm-6 col-md-3">
                    <div id="myCarouse{{$i}}" class="carousel slide" data-ride="carousel">
                        <ol class="carousel-indicators">
                            <li data-target="#myCarouse{{$i}}" data-slide-to="0" class="active"></li>
                        </ol>
                        <div class="carousel-inner">
                            @foreach(explode(';', $product->photo) as $key => $photo)
                            <div class="carousel-item {{$key == 0 ? 'active' : '' }}">
                                <div class="d-flex justify-content-center">
                                    <img src="{{ $photo }}" style="height: 300px !important;">
                                </div>
                            </div>
                            @endforeach
                        </div>
                        <a class="carousel-control-prev" href="#myCarouse{{$i}}" role="button"  data-slide="prev">
                            <span class="carousel-control-prev-icon" aria-hidden="true">     </span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="carousel-control-next" href="#myCarouse{{$i}}" role="button" data-slide="next">
                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                        </a>
                    </div>
                    <div class="caption">
                        <h4>{{ $product->name }}</h4>
                        <p>{{ Str::limit(Str::lower($product->description), 50) }}</p>
                        <p><strong>Price: </strong> {{ $product->price }} руб.</p>
                        <p class="btn-holder"><a href="{{ url('add-to-cart/'.$product->id) }}" class="btn btn-warning btn-block text-center" role="button">Add to cart</a> </p>
                    </div>
                </div>
                @php
                    $i++;
                @endphp
            @endforeach

        </div><!-- End row -->

    </div>

@endsection
