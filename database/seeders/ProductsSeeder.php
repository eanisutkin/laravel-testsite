<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProductsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('products')->insert([
            'name' => 'Samsung Galaxy S9',
            'description' => 'A new Lilac Purple S9, 128 gb',
            'quantity' => 16,
            'price' => 17898.99,
            'category'=>'smartphone;samsung;galaxy;s_series;s_9',
            'photo' => 'assets/img/samsung_gs9.jpg;assets/img/samsung_gs9_1.jpg;samsung_gs9_2.jpg'
        ]);

        DB::table('products')->insert([
            'name' => 'Samsung Galaxy S8',
            'description' => 'Black s8, 64 gb',
            'quantity' => 13,
            'price' => 14487.11,
            'category'=>'smartphone;samsung;galaxy;s_series;s_8',
            'photo' => 'assets/img/samsung_gs8.jpg;assets/img/samsung_gs8_1.jpg;samsung_gs8_2.jpg'
        ]);

        DB::table('products')->insert([
            'name' => 'Samsung Galaxy S7',
            'description' => 'Blue s7 32 gb',
            'quantity' => 18,
            'price' => 19999.99,
            'category'=>'smartphone;samsung;galaxy;s_series;s_7',
            'photo' => 'assets/img/samsung_gs7.jpg;assets/img/samsung_gs7_1.jpg'
        ]);

        DB::table('products')->insert([
            'name' => 'Samsung A71',
            'description' => 'Blue A71',
            'quantity' => 2,
            'price' => 21000.22,
            'category'=>'smartphone;samsung;a_series;a_71',
            'photo' => 'assets/img/samsung_a71.jpg;assets/img/samsung_a71_1.jpg;samsung_a71_2.jpg'
        ]);

        DB::table('products')->insert([
            'name' => 'Samsung A70',
            'description' => 'Blue A70',
            'quantity' => 1,
            'price' => 13999.99,
            'category'=>'smartphone;samsung;a_series;a_70',
            'photo' => 'assets/img/samsung_a70.jpg'
        ]);

        DB::table('products')->insert([
            'name' => 'Apple Iphone 13',
            'description' => 'New',
            'quantity' => 19,
            'price' => 89998.99,
            'category'=>'smartphone;apple;13',
            'photo' => 'assets/img/iphone_13.jpg;assets/img/ipone_13_1.jpg'
        ]);

        DB::table('products')->insert([
            'name' => 'Apple Iphone 12',
            'description' => 'New',
            'quantity' => 11,
            'price' => 68998.88,
            'category'=>'smartphone;apple;12',
            'photo' => 'assets/img/iphone_12.jpg;assets/img/iphine_12_1.jpg'
        ]);

        DB::table('products')->insert([
            'name' => 'Apple Iphone 11',
            'description' => 'New',
            'quantity' => 6,
            'price' => 48698.88,
            'category'=>'smartphone;apple;11',
            'photo' => 'assets/img/iphone_11.jpg;assets/img/iphine_11_1.jpg'
        ]);

        DB::table('products')->insert([
            'name' => 'Samsung a51',
            'description' => 'Used',
            'quantity' => 4,
            'price' => 15000.00,
            'category'=>'smartphone;samsung;a_series;a_51',
            'photo' => 'assets/img/samsung_a51.jpg;assets/img/samsung_a51_1.jpg;assets/img/samsung_a51_2.jpg'
        ]);

        DB::table('products')->insert([
            'name' => 'Samsung a50',
            'description' => 'Used',
            'quantity' => 13,
            'price' => 11000.00,
            'category'=>'smartphone;samsung;a_series;a_50',
            'photo' => 'assets/img/samsung_a50.jpg'
        ]);

        DB::table('products')->insert([
            'name' => 'Xiaomi mi 11',
            'description' => 'New',
            'quantity' => 7,
            'price' => 30999,
            'category'=>'smartphone;xiaomi;mi;11',
            'photo' => 'assets/img/xiaomi_mi11.jpg;assets/img/xiaomi_mi11_1.jpg'
        ]);

        DB::table('products')->insert([
            'name' => 'Xiaomi note 10',
            'description' => 'New',
            'quantity' => 3,
            'price' => 14000.11,
            'category'=>'smartphone;xiaomi;note;10',
            'photo' => 'assets/img/xiaomi_note10.jpg;assets/img/xiaomi_note10_1.jpg;assets/img/xiaomi_note10_2.jpg'
        ]);

        DB::table('products')->insert([
            'name' => 'Nokia 106',
            'description' => 'New',
            'quantity' => 4,
            'price' => 1350.99,
            'category'=>'phone;nokia;106',
            'photo' => 'assets/img/nokia_106.jpg;assets/img/nokia_106_1.jpg'
        ]);

        DB::table('products')->insert([
            'name' => 'Philips Xenium E111',
            'description' => 'New',
            'quantity' => 1,
            'price' => 1298.99,
            'category'=>'phone;philips;xenium;e111',
            'photo' => 'assets/img/philips_xe111.jpg;assets/img/philips_xe111_1.jpg'
        ]);
    }

}
